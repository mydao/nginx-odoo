FROM nginx:stable

MAINTAINER Andrii Aheiev <a.ageyev@gmail.com>

RUN ln -sf /dev/stdout /var/log/nginx/access.log && ln -sf /dev/stderr /var/log/nginx/error.log

COPY ssl/ /etc/nginx/ssl/
COPY conf.d/ssl.conf /etc/nginx/conf.d/ssl.conf
COPY odoo.conf /etc/nginx/conf.d/default.conf

EXPOSE 80
EXPOSE 443
