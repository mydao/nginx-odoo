### Build image

```bash
docker build -t mydao/nginx-odoo:mydao.me_2018-06-03 .
docker push mydao/nginx-odoo:mydao.me_2018-06-03
```

### Test

```bash
docker run -it --rm mydao/nginx-odoo:youko.co bash
```
